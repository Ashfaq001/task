import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class DataScreen extends StatefulWidget {
  String username, password;
  DataScreen({required this.username, required this.password});

  @override
  State<DataScreen> createState() => _DataScreenState();
}

class _DataScreenState extends State<DataScreen> {
  var apiresponse;
  @override
  void initState() {
    super.initState();
    apiresponse = getdata();
  }

  var data;
  getdata() async {
    print("ashfaq");
    Uri url = Uri.parse("https://reqres.in/api/users");
    print("ashfaq1");

    var response = await http.get(url);
    print("ashfaq2");

    print(response.body);

    setState(() {
      data = json.decode(response.body);
    });
    return data;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.lightBlue[100],
      appBar: AppBar(
        title: Text("data"),
      ),
      drawer: Drawer(
        backgroundColor: Colors.lightBlue[100],
        child: Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    "Username:${widget.username}",
                    style: TextStyle(fontSize: 18),
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    "Password:${widget.password}",
                    style: TextStyle(fontSize: 18),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      body: Container(
        child: FutureBuilder(
          future: apiresponse,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Container(
                height: MediaQuery.of(context).size.height * .98,
                width: MediaQuery.of(context).size.width,
                child: ListView.builder(
                    itemCount: data["data"].length,
                    itemBuilder: (context, index) {
                      return Container(
                        child: Padding(
                          padding: const EdgeInsets.only( right: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(data["data"][index]["first_name"]),
                              Text(data["data"][index]["last_name"]),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 20.0, top: 15),
                                child: Container(
                                  height: 50,
                                  width: 50,
                                  child: Image.network(
                                      data["data"][index]["avatar"]),
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    }),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}
